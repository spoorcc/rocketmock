#ifndef ROCKET_MOCK_H__
#define ROCKET_MOCK_H__

/*!
 * \addtogroup RocketMock-library
 *  @{
 */

/*!
 * \file RocketMock.h
 *
 * \brief RocketMock is the core mock object to use
 */

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

#ifdef  __cplusplus
extern "C" {
#endif

typedef enum {
	ROCKET_EXPECTATION_UNSET = 0u,       /*!< No expectation was set */
	ROCKET_EXPECTATION_MET,              /*!< Expectation was met */
	ROCKET_EXPECTATION_NOT_MET,          /*!< Expectation was not met */
	ROCKET_EXPECTATION_CANDIDATE,        /*!< Expectation was not met (yet) */
	ROCKET_EXPECTATION_NO_CANDIDATE,     /*!< Expectation was not met (yet) */
} RocketMockExpectationState_t;

/*!
 * \brief A struct for providing a buffer
 */
typedef struct {
	uint8_t const * data; /*!< Pointer buffer */
	size_t size;          /*!< Size of buffer */
} RocketMockBuffer_t;

/*!
 * \brief A common result enum
 * 
 */
typedef enum {
	ROCKET_NO_ERROR = 0u  /*!< No error, everything went OK */
} RocketMockResult_t;

/*!
 * \brief Reaction function prototype
 * 
 * \param[in]  bufferIn   Input buffer received
 * \param[out] bufferOut  Output buffer to be filled by Reaction
 * 
 * A reaction can take the bufferIn and calculate a reaction. This is expected in bufferOut. 
 * If there is a reaction, it will be forwarded to the registered dataSink.
 * 
 * \memberof RocketMockAction_t
 * 
 * \see RocketMock_t::set_sink
 */
typedef RocketMockResult_t (RocketMockReaction_t)(RocketMockBuffer_t const * const bufferIn,
											      RocketMockBuffer_t * const bufferOut);
											  
/*!
 * \brief A enum to distinguish between different kind of RocketMockAction_t actions
 *
 * \private
 * \memberof RocketMockAction_t
 */
typedef enum {
	ROCKET_MOCK_NO_ACTION = 0u,  /*!< No action was set */
	ROCKET_MOCK_REACTION         /*!< A RocketMockReaction_t was set */
} RocketMockActionType_t;

/*!
 * \brief A union to hold different kind of RocketMockAction_t actions
 *
 * \private
 * \memberof RocketMockAction_t
 */
typedef union {
	RocketMockReaction_t * _reaction; /*!< A RocketMockReaction_t was set \private */
} RocketMockActionUnion_t;

/*!
 * \brief An action that can be taken.
 *
 * \memberof RocketMockAction_t
 */
typedef struct {
	RocketMockActionUnion_t _u;   /*!< \private */
	RocketMockActionType_t _type; /*!< \private */
}RocketMockAction_t;		

/*!
 * \brief Set a RocketMockReaction_t as action, and REACT to any met expectation
 * 
 * \param rocketMockReactPtr Pointer to RocketMockReaction_t
 * 
 * \memberof RocketMockAction_t
 */
#define REACT(rocketMockReactPtr) (RocketMockAction_t) {&rocketMockReactPtr, ROCKET_MOCK_REACTION }	

/*! \private */
typedef enum {
	ROCKET_DATA_MATCHER = 0u
} RocketMockMatcherType_t;

/*!
 * \brief  A generic matcher struct
 * 
 */
typedef struct RocketMockMatcher_t RocketMockMatcher_t;

/*!
 * \brief Reset matcher
 * 
 * \memberof RocketMockMatcher_t 
 */
typedef void (RocketMockMatcherReset_t)(RocketMockMatcher_t* const);

/*!
 * \brief Perform matching
 * 
 * \memberof RocketMockMatcher_t 
 */
typedef RocketMockExpectationState_t (RocketMockMatcherMatch_t)(RocketMockMatcher_t* const,
	                                                            uint8_t const ** dataIterator,
                                                                uint8_t const * const dataEnd);
																
/*! \private */
struct RocketMockMatcher_t {
	RocketMockMatcherType_t type;         /*!< Type of matcher */
	RocketMockBuffer_t buf;               /*!< Data for matcher */
	uint8_t const * currentPosition;      /*!< Position in match data */
	RocketMockMatcherReset_t* reset;      /*!< Reset matcher */	
	RocketMockMatcherMatch_t* match;      /*!< Matching function */
};

/*! \private */
typedef struct RocketMockExpectationData_t RocketMockExpectationData_t;

/*! \private */
typedef void (RocketMockExpectationDataDestructor_t)(void*);

/*! \private */
struct RocketMockExpectationData_t{
    RocketMockMatcher_t _matcher;           /*!< The matcher of this expectation */

	uint32_t repeats;                       /*!< Number of times this expectation should repeat */      

	RocketMockAction_t _action;             /*!< Action to take each time the expectation is met */
	RocketMockExpectationState_t _state;	/*!< State of this expectation */

	RocketMockExpectationData_t * next;        /*!< Expectation after this */
	RocketMockExpectationData_t * alternative; /*!< Alternative expectation */
	RocketMockExpectationData_t * previous;    /*!< Parent expectation */

	RocketMockExpectationDataDestructor_t * destroy; /*!< Destructor of the data, if NULL, either on stack or responsibility of creator */
};



/**************************************/
/* Methods of RocketMockExpectation_t */

typedef struct RocketMockExpectation_t RocketMockExpectation_t;

/*!
 * \brief Set previous expectation again
 * 
 * \memberof RocketMockExpectation_t 
 */
typedef RocketMockExpectation_t const * const (RocketMockAgain_t)(void);

/*!
 * \brief Repeat last expectation n times
 * 
 * \param[in] count   Number of times to expect previous expectation
 * 
 * \memberof RocketMockExpectation_t 
 */
typedef RocketMockExpectation_t const * const (RocketMockNTimes_t)(const uint32_t count);

/*!
 * \brief Set an action when an full expectation is met
 * 
 * \param[in] action   Action to perform, when expectation is met (e.g. \ref REACT)
 * 
 * \memberof RocketMockExpectation_t
 */
typedef RocketMockExpectation_t const * const (RocketMockWill_t)(RocketMockAction_t action);

/*!
 * \brief Make the expectation follow after other expectation
 * 
 * \param[in] expectation   Expectation to be after
 * 
 * \memberof RocketMockExpectation_t
 */
typedef RocketMockExpectation_t const * const (RocketMockAfter_t)(RocketMockExpectationData_t * expectation);

struct RocketMockExpectation_t {
	RocketMockAgain_t * again;        /*!< \copydoc RocketMockAgain_t*/
	RocketMockNTimes_t * n_times;     /*!< \copydoc RocketMockNTimes_t */
	RocketMockWill_t * will;          /*!< \copydoc RocketMockWill_t */
	RocketMockAfter_t * after;        /*!< \copydoc RocketMockAfter_t */
	
	RocketMockExpectationData_t * _data;        /*!< \private */
};

typedef struct RocketMock_t RocketMock_t;

/* Methods of RocketMock */

/*!
 * \brief Reset mock to default state
 * 
 * \memberof RocketMock_t
 */
typedef RocketMock_t * const (RocketMockReset_t)(void);

/*!
 * \brief Set expectation on a mock
 * 
 * \param[in] expectation Pointer to the expectation to be met (Created with \ref EXPECTATION)).
 * 
 * As data is fed later on, the expectation will be checked byte-per-byte.
 * 
 * ### Example
 * To set an expectation, first create one with the \ref EXPECTATION macro,
 * after that register the expectation using \ref expect.
 * 
 * \snippet test_RocketMock.cpp Setting expectation
 * 
 * \memberof RocketMock_t
 * \see EXPECTATION
 */
typedef RocketMockExpectation_t const * const (RocketMockExpect_t)(RocketMockExpectationData_t* const expectation);

/*!
 * \brief Feed the mock with data
 * 
 * \param[in] data  Buffer with data to feed to the mock
 * \param[in] size  Size of the data buffer provided
 * 
 * \note The data can be fed into the chunks as they come in.
 * 
 * ### Example
 * To feed the mock with data, provide it with a buffer and its size to \ref feed.
 * \snippet test_RocketMock.cpp Feeding data
 * 
 * \memberof RocketMock_t 
 */
typedef RocketMock_t const * const (RocketMockFeed_t)(uint8_t const * const data, size_t size);

/*!
 * \brief Check if all the set expectations were met
 * 
 * After an expectation was set with \ref expect and data was fed through \ref feed,
 * the \ref check function can be used to check if the set expectation was met.
 * 
 * \memberof RocketMock_t 
 */
typedef RocketMockExpectationState_t (RocketMockCheck_t)(void);

/*!
 * \brief Check if a specific expectation is met
 * 
 * \param [in] expectation  Pointer to RocketMockExpectationData_t created with EXPECTATION
 * 
 * After an expectation was set with \ref expect and data was fed through \ref feed,
 * the \ref check function can be used to check if a specific expectation was met.
 * 
 * \memberof RocketMock_t 
 */
typedef RocketMockExpectationState_t (RocketMockCheckExpectation_t)(RocketMockExpectationData_t const * const expectation);

/*!
 * \brief Type for sinking data to from the mock
 * 
 * \param[in] buffer Pointer to RocketMockBuffer_t
 * 
 * \memberof RocketMock_t 
 */
typedef void (RocketMockSink_t)(RocketMockBuffer_t const * const buffer);

/*!
 * \brief Set the datasink
 * 
 * \param[in] sink Pointer to a RocketMockSink_t
 * 
 * \memberof RocketMock_t 
 */
typedef RocketMock_t const * const (RocketMockSetSink_t)(RocketMockSink_t * const sink);


/* Rocket Mock */
struct RocketMock_t{
	RocketMockReset_t * reset;                         /*!< \copydoc RocketMockReset_t*/
	RocketMockExpect_t * expect;                       /*!< \copydoc RocketMockExpect_t*/
	RocketMockFeed_t * feed;                           /*!< \copydoc RocketMockFeed_t*/
	RocketMockCheck_t * check;                         /*!< \copydoc RocketMockCheck_t*/
	RocketMockCheckExpectation_t * checkExpectation;   /*!< \copydoc RocketMockCheckExpectation_t*/
	RocketMockSetSink_t * set_sink;                    /*!< \copydoc RocketMockSetSink_t*/

	RocketMockExpectation_t _expectation; /*!< \private */

	RocketMockSink_t * _dataSink; /*!< \private */
};

/* Public defines */
#define DATA(...) __VA_ARGS__ 

/*!
 * \brief Create an expectation
 * 
 * \param NAME      Name of your expectation
 * \param RAW_DATA  Data you are expecting
 * 
 * \see RocketMockExpect_t
 */
#ifndef __cplusplus
#define EXPECTATION(NAME, RAW_DATA) uint8_t NAME##_data[] = RAW_DATA; \
							RocketMockExpectationData_t NAME = {.buf = {.data = NAME##_data,                        \
							                                    .size = sizeof(NAME##_data)/sizeof(NAME##_data[0])  \
																  }, \
														   .repeats=0u };
#else
#define EXPECTATION(NAME, RAW_DATA)                                                \
    uint8_t NAME##_data[] = RAW_DATA;                                              \
                                                                                   \
	RocketMockMatcher_t NAME##_matcher = [](uint8_t* data, size_t size){           \
	       RocketMockMatcher_t m = RocketMockMatcher_t();                          \
		   m.type = ROCKET_DATA_MATCHER;                                           \
		   m.buf.data = data;                                                      \
		   m.buf.size = size;                                                      \
		   return m;                                                               \
	}( NAME##_data, sizeof(NAME##_data)/sizeof(NAME##_data[0]) );                  \
	                                                                               \
	RocketMockExpectationData_t NAME = [](RocketMockMatcher_t matcher){            \
		RocketMockExpectationData_t e = RocketMockExpectationData_t();             \
		e.repeats = 0u;                                                            \
		e._matcher = matcher;                                                      \
		return e;                                                                  \
	}(NAME##_matcher);
#endif

/* Public functions */
RocketMock_t const * const Rocket(RocketMock_t * rocket);

/** @}*/

#ifdef  __cplusplus
}
#endif

#endif
