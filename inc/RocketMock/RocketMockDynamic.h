#ifndef ROCKET_MOCK_DYNAMIC_H__
#define ROCKET_MOCK_DYNAMIC_H__

/*!
 * \addtogroup RocketMockDynamic-library
 *  @{
 */

/*!
 * \file RocketMockDynamic.h
 *
 * \brief RocketMockDynamic makes it possible to use RocketMocks from the heap
 */

#include <RocketMock/RocketMock.h>

#ifdef  __cplusplus
extern "C" {
#endif

#define RAW_UINT8(...) (const uint8_t[]){__VA_ARGS__}, sizeof((const uint8_t[]){__VA_ARGS__})/sizeof(uint8_t)

RocketMock_t* RocketMock_Create(void);
void RocketMock_Destroy(RocketMock_t** mock);

RocketMockExpectationData_t* Data(const uint8_t data[], size_t size);

/** @}*/

#ifdef  __cplusplus
}
#endif

#endif
