#include <malloc.h>

#include <RocketMock/RocketMockDynamic.h>

#ifdef __cplusplus
extern "C" { 
#endif
static void RocketMockDataFree(void* data);
#ifdef __cplusplus
}
#endif

RocketMock_t* RocketMock_Create(void)
{
    RocketMock_t* mock = (RocketMock_t *) malloc(sizeof(RocketMock_t));

    if(mock)
    {
        Rocket(mock);
    }

    return mock;
}

void RocketMock_Destroy(RocketMock_t** mock)
{
    if(*mock != NULL)
    {
        RocketMockExpectationData_t* data = (*mock)->_expectation._data;
        RocketMockExpectationData_t* previous = NULL;

        while(data != NULL)
        {
            /* Find deepest non-NULL data */
            while(data->alternative || data->next)
            {
                while(data->alternative)
                {
                    data = data->alternative;
                }

                if(data->next)
                {
                    data = data->next;
                }                
            }

            previous = data->previous;

            /* Destroy the data */
            if(data->destroy)
            {
                data->destroy((void*)data);
            }

            /* Mark destroyed data as NULL */
            if(previous)
            {
                if(data == previous->next)
                {
                    previous->next = NULL;
                }
                else
                {
                    previous->alternative = NULL;
                }
            }

            /* rinse, wash, repeat */
            data = previous;
        }

        free(*mock);

        *mock = NULL;
    }
}

RocketMockExpectationData_t* Data(const uint8_t data[], size_t size)
{
    RocketMockExpectationData_t* expectation = NULL;

    if(size > 0u)
    {
        expectation = (RocketMockExpectationData_t*) calloc(0u, sizeof(RocketMockExpectationData_t));

        if(expectation)
        {
            expectation->_matcher.buf.data = data;
            expectation->_matcher.buf.size = size;
        }

        expectation->destroy = &RocketMockDataFree;
    }

    return expectation;
}

static void RocketMockDataFree(void* data) 
{
    free(data);
}
