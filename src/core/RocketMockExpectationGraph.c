
#include "RocketMockExpectationGraph.h"

void _rocket_add_alternative(RocketMockExpectationData_t * start,
                             RocketMockExpectationData_t * expectation )
{
    RocketMockExpectationData_t * alternativeHolder = start;

    while(alternativeHolder->alternative != NULL)
    {
        alternativeHolder = alternativeHolder->alternative;
    } 
    
    alternativeHolder->alternative = expectation;
    expectation->previous = alternativeHolder;
}

void _rocket_insert(RocketMockExpectationData_t ** current,
                    RocketMockExpectationData_t * previous,
                    RocketMockExpectationData_t * const newExpectation )
{
    if(*current == NULL)
    {
        *current = newExpectation;
        newExpectation->previous = previous;
    }
    else
    {
       _rocket_add_alternative(*current, newExpectation);
    }
}

void _rocket_remove_expectation(RocketMockExpectationData_t * nodeToRemove)
{
    if(nodeToRemove->previous->next == nodeToRemove)
    {
        nodeToRemove->previous->next = NULL;
    }
    else 
    {
        RocketMockExpectationData_t * alternativeHolder = nodeToRemove->previous;

        while((alternativeHolder != NULL) && (alternativeHolder->alternative != nodeToRemove))
        {
            alternativeHolder = alternativeHolder->alternative;
        }

        if(alternativeHolder != NULL)
        {
            alternativeHolder->alternative = nodeToRemove->alternative;
        }
    }

    /* TODO: Recouple the nodeToRemove->next AND nodeToRemove->alternative to nodeToRemove->previous */
}

/*!
 * \brief Traverse through graph starting from _current and find next node that matches according to the provided matcher
 * 
 * \param _current   Node to start the traversal from.
 * \param matcher    _rocket_is_node_t matcher that return is current node matches
 * 
 * \retval  NULL  No RocketMockExpectationData_t found that matches matcher
 * \retval  other Pointer to RocketMockExpectationData_t that matches matcher
 */
RocketMockExpectationData_t* _find_expectation_data(RocketMockExpectationData_t * _current, _rocket_is_node_t* matcher)
{
    RocketMockExpectationData_t * next = NULL;

    do
    {
        /* Always prefer the next node */
        if((_current->next != NULL) && (_current->_state == ROCKET_EXPECTATION_MET))
        {
            next = _current->next;
        }
        /* Otherwise check if current has a alternative node */
        else if(_current->alternative != NULL)
        {
            next = _current->alternative;
        }
        /* If all else fails, step back to previous untill new alternative is found */
        else
        {
            next = NULL;
            
            /* Try to find first previous with a new alternative */
            while(_current->previous)
            {               
                if ((_current->previous->alternative) && (_current->previous->alternative != _current))
                {
                    next = _current->previous->alternative;
                    break;
                }

                _current = _current->previous;
            }
        }

        _current = next;

    } while((next != NULL) && (!matcher(next))); /* Did we found a match or did we found nothing */
    
    return next;
}
