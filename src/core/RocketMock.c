#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

#include "RocketMockMatchers.h"
#include "RocketMockExpectationGraph.h"

/* Public interface */
#include "RocketMock/RocketMock.h"

/* Private interface */
#define ROCKET_CHECK_PARAMETER(cond)  RocketMock_OnAssert(cond);
#define ROCKET_PRECONDITION(cond)     RocketMock_OnAssert(cond);
#define ROCKET_POSTCONDITION(cond)    RocketMock_OnAssert(cond);

static RocketMock_t * const RocketInit(RocketMock_t * const rocket);

static RocketMock_t * const _rocket_reset_impl(void);
static RocketMockExpectation_t const * const _rocket_expect_impl(RocketMockExpectationData_t* const expectation);
static RocketMock_t const * const _rocket_feed_impl(uint8_t const * const data, size_t size);
static void _rocket_perform_actions(RocketMockExpectationData_t* expectationData);
static RocketMockExpectationState_t _rocket_check_impl(void);
static RocketMockExpectationState_t _rocket_check_expectation_impl(RocketMockExpectationData_t const * const expectation);
static RocketMock_t const * const _rocket_set_sink_impl(RocketMockSink_t * const sink);

static RocketMockExpectation_t const * const _rocket_again_impl(void);
static RocketMockExpectation_t const * const _rocket_n_times_impl(const uint32_t count);
static RocketMockExpectation_t const * const _rocket_will_impl(RocketMockAction_t action);
static RocketMockExpectation_t const * const _rocket_after_impl(RocketMockExpectationData_t * expectation);

/*!
 * \brief Weak assert handler
 * 
 */
#pragma weak RocketMock_OnAssert
void RocketMock_OnAssert(int cond) { assert(cond); }

/* Module globals */
static RocketMock_t* gRocket = NULL;
static RocketMockExpectation_t* gExpectation = NULL;
static RocketMockExpectationData_t* gExpectationData = NULL;

/** \addtogroup RocketMock-library
 *  Library for Mocking like a rocket
 * 
 * # Basic Usage
 * 
 * - Create expectations with EXPECTATION(<name_of_expectation._data>, DATA(<data>))
 * - Set expectations using \ref RocketMock_t::expect
 * - Feed the mock data from somewhere using \ref RocketMock_t::feed
 * - Check if all expectations were met using \ref RocketMock_t::check == ROCKET_expectation._data_MET
 *
 *  @{
 */

/*!
 * \brief Get access to your rocket
 * 
 * \param rocket   Pointer to initialized Rocket memory
 * \return Rocket to call your functions on
 */
RocketMock_t const * const Rocket(RocketMock_t * const rocket)
{
    ROCKET_CHECK_PARAMETER(rocket != NULL);

    if(rocket->reset != _rocket_reset_impl)
    {
        gRocket = RocketInit(rocket);
    }
    else
    {
        gRocket = rocket;
    }

    return gRocket;
}

/* Private functions */
static RocketMock_t * const _rocket_reset_impl(void)
{
    ROCKET_PRECONDITION(gRocket != NULL);

    gRocket->_expectation._data = NULL;
    return gRocket;
}

static void _rocket_init_expectation_data(RocketMockExpectationData_t * const expectation)
{
    _rocket_init_matcher(&expectation->_matcher);

    expectation->_state = ROCKET_EXPECTATION_CANDIDATE;
    expectation->alternative = NULL;
    expectation->previous = NULL;
    expectation->next = NULL;
}

static RocketMockExpectation_t const * const _rocket_expect_impl(RocketMockExpectationData_t * const expectation)
{
    ROCKET_PRECONDITION(gRocket != NULL);
    ROCKET_CHECK_PARAMETER(expectation != NULL);

    _rocket_init_expectation_data(expectation);
    _rocket_insert(&gRocket->_expectation._data, NULL, expectation);

    gExpectation = &gRocket->_expectation;
    gExpectationData = expectation;

    ROCKET_POSTCONDITION(gExpectationData != NULL);
    ROCKET_POSTCONDITION(gRocket->_expectation._data != NULL);

    return &gRocket->_expectation;
}

static RocketMockExpectation_t const * const _rocket_again_impl(void)
{
    ROCKET_PRECONDITION(gExpectationData != NULL);

    gExpectationData->repeats += 1u;

    return gExpectation;
}

static RocketMockExpectation_t const * const _rocket_n_times_impl(const uint32_t count)
{
    ROCKET_CHECK_PARAMETER(count > 0u);
    ROCKET_PRECONDITION(gExpectation != NULL);

    if(gExpectation->_data->repeats)
    {
        gExpectation->_data->repeats += count;
    }
    else
    {
        gExpectation->_data->repeats = (count - 1u);
    }

    return gExpectation;
}

static RocketMockExpectation_t const * const _rocket_will_impl(RocketMockAction_t action)
{
    gExpectationData->_action = action;

    return gExpectation;
}


static RocketMockExpectation_t const * const _rocket_after_impl(RocketMockExpectationData_t * expectation)
{
    ROCKET_PRECONDITION(gExpectationData != NULL);
    ROCKET_PRECONDITION(expectation != NULL);

    _rocket_remove_expectation(gExpectationData);
    _rocket_insert(&expectation->next, expectation, gExpectationData);

    return gExpectation;
}

static bool _is_candidate(RocketMockExpectationData_t const * _expectation)
{
    return (_expectation->_state == ROCKET_EXPECTATION_CANDIDATE);
}

static RocketMockExpectationData_t * _get_next_candidate(RocketMockExpectationData_t * _current)
{
    return _find_expectation_data(_current, _is_candidate);
}

static bool _rocket_is_no_candidate(RocketMockExpectationData_t const * _current)
{
    return _current->_state == ROCKET_EXPECTATION_NO_CANDIDATE;
}

static void _rocket_reset_any_no_candidate_expectations(void)
{
    RocketMockExpectationData_t * _expectation = gRocket->_expectation._data;
    while(_expectation) 
    {
        if(_rocket_is_no_candidate(_expectation))
        {
            _expectation->_state = ROCKET_EXPECTATION_CANDIDATE;
            _expectation->_matcher.reset(&_expectation->_matcher);
        }

        _expectation = _find_expectation_data(_expectation, &_rocket_is_no_candidate);
    }
}

static void _rocket_fail_any_no_candidate_expectations(void)
{
    RocketMockExpectationData_t * _expectation = gRocket->_expectation._data;
    while(_expectation) 
    {
        if(_rocket_is_no_candidate(_expectation))
        {
            _expectation->_state = ROCKET_EXPECTATION_NOT_MET;
        }

        _expectation = _find_expectation_data(_expectation, &_rocket_is_no_candidate);
    }
}

static void _rocket_decrement_any_repeats(RocketMockExpectationData_t * expectation)
{
    if(expectation->repeats)
    {
        expectation->repeats -= 1u;
        expectation->_state = ROCKET_EXPECTATION_CANDIDATE;
        expectation->_matcher.reset(&expectation->_matcher);
    }
}

static RocketMock_t const * const _rocket_feed_impl(uint8_t const * const data, size_t size)
{
    ROCKET_CHECK_PARAMETER(data != NULL);
    ROCKET_PRECONDITION(gRocket != NULL);

    uint8_t const * dataIterator = data;
    uint8_t const * const dataEnd = (uint8_t const * const ) data + size;

    RocketMockExpectationData_t * _currentData = gRocket->_expectation._data;

    while((_currentData != NULL) && (dataIterator != dataEnd))
    {
        if(_currentData->_state == ROCKET_EXPECTATION_CANDIDATE)
        {
            uint8_t const * dataIteratorStart = dataIterator;

            _currentData->_state = _currentData->_matcher.match(&_currentData->_matcher, &dataIterator, dataEnd);

            if(_currentData->_state == ROCKET_EXPECTATION_MET)
            {
                _rocket_perform_actions(_currentData);
                _rocket_decrement_any_repeats(_currentData);
                _rocket_reset_any_no_candidate_expectations();
            }
            else if(_currentData->_state == ROCKET_EXPECTATION_NO_CANDIDATE)
            {
                /* Reset any changes by matcher */
                dataIterator = dataIteratorStart;
            }            
        }
        else
        {
            _currentData = _get_next_candidate(_currentData);

            if(!_currentData)
            {
                _rocket_fail_any_no_candidate_expectations();
            }
        }        
    }

    return gRocket;
}

static void _rocket_perform_actions(RocketMockExpectationData_t* expectationData)
{
    if(expectationData->_action._type != ROCKET_MOCK_NO_ACTION)
    {
        RocketMockBuffer_t bufOut = {0u};

        /* TODO: Remove dependency on _data matcher internal data buffer */
        RocketMockResult_t actionResult = expectationData->_action._u._reaction(&expectationData->_matcher.buf, &bufOut);

        if((gRocket->_dataSink != NULL) && (actionResult == ROCKET_NO_ERROR))
        {
            gRocket->_dataSink(&bufOut);
        }
    }
}

static bool _rocket_check_if_not_met(RocketMockExpectationData_t const * _expectation)
{
    return _expectation->_state != ROCKET_EXPECTATION_MET;
}

static RocketMockExpectationState_t _rocket_check_impl(void)
{
    ROCKET_PRECONDITION(gRocket != NULL);

    RocketMockExpectationState_t state;

    if (gRocket->_expectation._data == NULL)
    {
        state = ROCKET_EXPECTATION_UNSET;
    }
    else
    {
        if(gRocket->_expectation._data->_state == ROCKET_EXPECTATION_MET)
        {
            if(_find_expectation_data(gRocket->_expectation._data, &_rocket_check_if_not_met) != NULL)
            {
                state = ROCKET_EXPECTATION_NOT_MET;
            }
            else
            {
                state = ROCKET_EXPECTATION_MET;
            }
        }
        else
        {
            state = ROCKET_EXPECTATION_NOT_MET;
        }
    }

    return state;
}

static RocketMockExpectationState_t _rocket_check_expectation_impl(RocketMockExpectationData_t const * const expectation)
{
    ROCKET_CHECK_PARAMETER(expectation != NULL);

    return expectation->_state;
}

static RocketMock_t const * const _rocket_set_sink_impl(RocketMockSink_t * const sink)
{
    ROCKET_PRECONDITION(gRocket != NULL);
    ROCKET_CHECK_PARAMETER(sink != NULL);

    gRocket->_dataSink = sink;   
 
    return gRocket;
}

/*!
 * \brief Initialize your rocket
 * 
 * \param rocket   Pointer to memory which should be initialized as rocket
 * \return Pointer to the initialized rocket.
 */
static RocketMock_t * const RocketInit(RocketMock_t * const rocket)
{
    rocket->reset = &_rocket_reset_impl;
    rocket->expect = &_rocket_expect_impl;
    rocket->feed = &_rocket_feed_impl;
    rocket->check = &_rocket_check_impl;
    rocket->checkExpectation = &_rocket_check_expectation_impl;
    rocket->set_sink = &_rocket_set_sink_impl;

    rocket->_expectation.again = &_rocket_again_impl;
    rocket->_expectation.n_times = &_rocket_n_times_impl;
    rocket->_expectation.will = &_rocket_will_impl;
    rocket->_expectation.after = &_rocket_after_impl;

    rocket->_dataSink = NULL;

    return Rocket(rocket)->reset();
}

/** @}*/
