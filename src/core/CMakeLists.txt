
if (BUILD_AVR)
    add_avr_library(RocketMock STATIC RocketMock.c RocketMockMatchers.c RocketMockExpectationGraph.c)
elseif(BUILD_MSP430)
    msp430_add_library_compilation(RocketMock STATIC RocketMock.c RocketMockMatchers.c RocketMockExpectationGraph.c)
else()
    add_library(RocketMock SHARED RocketMock.c RocketMockMatchers.c RocketMockExpectationGraph.c)
endif()