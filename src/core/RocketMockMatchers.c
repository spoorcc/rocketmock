#include <assert.h>

#include "RocketMockMatchers.h"

static void _rocket_reset_data_matcher(RocketMockMatcher_t * const dataMatcher);
static RocketMockExpectationState_t _rocket_do_data_matching(RocketMockMatcher_t * const dataMatcher,
                                                             uint8_t const ** dataIterator, 
                                                             uint8_t const * const dataEnd);


void _rocket_init_matcher(RocketMockMatcher_t * const matcher)
{
    switch(matcher->type)
    {
        case ROCKET_DATA_MATCHER:
            matcher->reset = &_rocket_reset_data_matcher;
            matcher->match = &_rocket_do_data_matching;
            break;
        default:
            assert(matcher->type == ROCKET_DATA_MATCHER);
            break;
    }

    matcher->reset(matcher);
}

static void _rocket_reset_data_matcher(RocketMockMatcher_t * const matcher)
{
    matcher->currentPosition = matcher->buf.data;
}

static RocketMockExpectationState_t _rocket_do_data_matching(RocketMockMatcher_t * const matcher, uint8_t const ** dataIterator, uint8_t const * const dataEnd)
{
    RocketMockExpectationState_t state = ROCKET_EXPECTATION_CANDIDATE;

    uint8_t const * const expectationEnd = (uint8_t const *) matcher->buf.data + 
                                                             matcher->buf.size;

    while((matcher->currentPosition != expectationEnd) && (*dataIterator != dataEnd))
    {
        if(*(*dataIterator)++ != *matcher->currentPosition++)
        {
            state = ROCKET_EXPECTATION_NO_CANDIDATE;
            matcher->currentPosition = matcher->buf.data;
            break;
        }
    }

    if((state != ROCKET_EXPECTATION_NO_CANDIDATE) && (matcher->currentPosition == expectationEnd))
    {
        state = ROCKET_EXPECTATION_MET;
    }

    return state;    
}