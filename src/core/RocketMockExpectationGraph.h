
#ifndef ROCKET_MOCK_EXPECTATION_GRAPH_H__
#define ROCKET_MOCK_EXPECTATION_GRAPH_H__

/*!
 * \addtogroup RocketMock-library
 *  @{
 */

/*!
 * \file RocketMockExpectationGraph.h
 *
 * \brief The expectation graph
 */

#include <stdint.h>
#include <stdbool.h>

#include <RocketMock/RocketMock.h>

#ifdef  __cplusplus
extern "C" {
#endif

void _rocket_add_alternative(RocketMockExpectationData_t * start,
                             RocketMockExpectationData_t * expectation );

void _rocket_insert(RocketMockExpectationData_t ** current,
                    RocketMockExpectationData_t * previous,
                    RocketMockExpectationData_t * const newExpectation );

void _rocket_remove_expectation(RocketMockExpectationData_t * nodeToRemove);

typedef bool (_rocket_is_node_t)(RocketMockExpectationData_t const *);

RocketMockExpectationData_t* _find_expectation_data(RocketMockExpectationData_t * _current, _rocket_is_node_t* matcher);

#ifdef  __cplusplus
}
#endif

#endif /* !ROCKET_MOCK_EXPECTATION_GRAPH_H__ */