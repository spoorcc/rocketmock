
#ifndef ROCKET_MOCK_MATCHERS_H__
#define ROCKET_MOCK_MATCHERS_H__

/*!
 * \addtogroup RocketMock-library
 *  @{
 * /
 */

/*!
 * \file RocketMock.h
 *
 * \brief RocketMock is the core mock object to use
 */

#include <stdint.h>
#include <stdarg.h>

#include <RocketMock/RocketMock.h>

#ifdef  __cplusplus
extern "C" {
#endif

void _rocket_init_matcher(RocketMockMatcher_t * const matcher);

#ifdef  __cplusplus
}
#endif

#endif /* !ROCKET_MOCK_MATCHERS_H__ */