[![pipeline status](https://gitlab.com/spoorcc/rocketmock/badges/master/pipeline.svg)](https://gitlab.com/spoorcc/rocketmock/commits/master)

# RocketMock
RocketMock is a mocking framework for datastreams on primarily embedded devices.
RocketMock makes it easy to create a physical mock for testing communication and interaction
between hardware systems and your Device under test. This is sometimes also known as _Hardware in the Loop_ testing.

See Documentation on [gitlab.io](https://spoorcc.gitlab.io/rocketmock). 
Or get the source code on [gitlab.com/spoorcc](https://gitlab.com/spoorcc/rocketmock).

## Main design goals

* No Dynamic memory
* A portable pure C Core
* Compilable for any platform
* Thread-safe (at some point :) )
* Complete documentation
* Extensible for your needs

## Basic Usage


### Verifying expectations

A RocketMock can be seen as a [Mock Object](http://xunitpatterns.com/Mock%20Object.html) and as such
the common flow of using mocks is applicable:

* Create a mock
* Set expectations (with `expect`)
* Let your _Device Under Test_ interact with the mock (with `feed`)
* Verify your expectations (with `verify`)

~~~~~~~~~~~~~~~{.c}
#include <RocketMock.h>

/* Define your RocketMock */
RocketMock_t myMock;

EXPECTATION(myExpectedMessage, DATA(0u, 1u,));

/* Create a RocketMock */
void init() {
    Rocket(&myMock)->expect(&myExpectedMessage);
}

/* Feed your RocketMock with data */
void i2c_receive_data(uint8_t incomingData[], size_t incomingDataSize){
    Rocket(&myMock)->feed(incomingData, incomingDataSize);
}

/* Check your expectation */
void check(void){
    Rocket(&myMock)->check();
}
~~~~~~~~~~~~~~~

### Reacting to inputs
If a RocketMock would only check incoming data, it wouldn't be very interesting. So RocketMock
gives you the opportunity to setup behavior as can be seen below with `will`.
In order to have a place to send its data, you must now also configure a datasink through `set_sink`.

~~~~~~~~~~~~~~~{.c}
#include <RocketMock.h>

/* Define your RocketMock */
RocketMock_t myMock;

/* Some external functions */
extern void i2c_send_data(uint8_t buf, size_t bufSize);
extern RocketMockResult_t i2c_calculate_response(RocketMockBuffer_t const *const bufferIn, RocketMockBuffer_t *const bufferOut);

EXPECTATION(myExpectedMessage, DATA(0u, 1u,));

/* Create a RocketMock and set your response */
void init() {
    Rocket(&myMock)->set_sink(&i2c_send_data);
    Rocket(&myMock)->expect(&myExpectedMessage)->will(REACT(i2c_calculate_response));
}

/* Feed your RocketMock with data */
void i2c_receive_data(uint8_t incomingData[], size_t incomingDataSize){
    Rocket(&myMock)->feed(incomingData, incomingDataSize);
}

/* Check your expectation */
void check(void){
    Rocket(&myMock)->check();
}
~~~~~~~~~~~~~~~

# Developing

## Compiling

Make sure to have CMake and a compiler installed.

~~~~~~~~~~~~~~~{.sh}
    mkdir bld
    cd bld
    cmake ..
    make
~~~~~~~~~~~~~~~

## Running tests

~~~~~~~~~~~~~~~{.sh}
    cd bld
    make test
~~~~~~~~~~~~~~~

For more verbose test output use CTest directly

~~~~~~~~~~~~~~~{.sh}
    ctest --verbose
~~~~~~~~~~~~~~~

## Generating documentation

The documentation is generated with Doxygen and CMake.

~~~~~~~~~~~~~~~{.sh}
    cd bld
    make doc
~~~~~~~~~~~~~~~

<center>
![http://pngimg.com/download/13289](doc/img/logo.png)
</center>