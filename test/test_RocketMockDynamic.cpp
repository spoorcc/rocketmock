/*!
 * \file test_RocketMock.c
 * \internal
 */


#include <cstring>

#include "CppUTest/TestHarness.h"
#include "CppUTest/CommandLineTestRunner.h"

#include "RocketMock/RocketMockDynamic.h"

#include <stdexcept>
extern "C" void RocketMock_OnAssert(int cond) { if(!cond) { throw std::runtime_error("assert"); } }

TEST_BASE(RocketMockDynamic) {
};

/*------------ Creating/ Detroying object -------------------*/
TEST_GROUP_BASE(RocketObject, RocketMockDynamic){};

TEST( RocketObject, test_rocket_can_be_created) {
    RocketMock_t* mock = RocketMock_Create();

    CHECK(mock != NULL);
    CHECK(mock->reset != NULL);
}

TEST( RocketObject, test_rocket_can_be_destroyed) {
    RocketMock_t* mock = RocketMock_Create();

    RocketMock_Destroy(&mock);

    POINTERS_EQUAL(NULL, mock);
}

TEST( RocketObject, test_rocket_with_single_dynamic_expectation_can_be_destroyed) {
    RocketMock_t* mock = RocketMock_Create();

    Rocket(mock)->expect(Data(RAW_UINT8(0u, 1u, 2u)));

    RocketMock_Destroy(&mock);
}

TEST( RocketObject, test_rocket_with_two_alternative_dynamic_expectation_can_be_destroyed) {
    RocketMock_t* mock = RocketMock_Create();

    Rocket(mock)->expect(Data(RAW_UINT8(0u, 1u, 2u)));
 //   Rocket(mock)->expect(Data(RAW_UINT8(1u, 3u, 5u)));
             
    RocketMock_Destroy(&mock);
}


/*------------ Creating expectations -------------------*/
TEST_GROUP_BASE(RocketDynamicExpectation, RocketMockDynamic){

    RocketMock_t mock;

    void setup() {
        resetMock();
    }

    void resetMock() {
        memset(&mock, 0, sizeof(mock));
    }
};

TEST( RocketDynamicExpectation, test_dynamic_expectation_can_be_used) {

    const uint8_t actualData[] = {0u, 1u};

    Rocket(&mock)->expect(Data(RAW_UINT8(0u, 1u)));

    Rocket(&mock)->feed(actualData, sizeof(actualData));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&mock)->check());
}

int main(int ac, char** av)
{
   return CommandLineTestRunner::RunAllTests(ac, av);
}