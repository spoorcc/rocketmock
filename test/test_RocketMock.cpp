/*!
 * \file test_RocketMock.c
 * \internal
 */


#include <cstring>

#include "CppUTest/TestHarness.h"
#include "CppUTest/CommandLineTestRunner.h"

#include "RocketMock/RocketMock.h"

#include <stdexcept>
extern "C" void RocketMock_OnAssert(int cond) { if(!cond) { throw std::runtime_error("assert"); } }

TEST_BASE(RocketMockCore) {
    RocketMock_t myMock;

    void setup() {
        resetMyMock();
    }

    void resetMyMock() {
        memset(&myMock, 0, sizeof(myMock) );
    }
};

/*------------ Object -------------------------*/
TEST_GROUP_BASE(RocketObject, RocketMockCore){};

TEST( RocketObject, test_expectation_is_unset) {
    UNSIGNED_LONGS_EQUAL( ROCKET_EXPECTATION_UNSET, Rocket(&myMock)->check() );
}

TEST( RocketObject, test_cannot_provide_null_pointer) {
    CHECK_THROWS(std::runtime_error, Rocket(NULL));
}

TEST( RocketObject, test_mock_unchanged_after_first_init) {
    RocketMock_t afterInit;

    /* Call it for first time */
    Rocket(&myMock);
    memcpy(&afterInit, &myMock, sizeof(RocketMock_t) );

    /* Call it for second time, and check it is unchanged */
    Rocket(&myMock);
    MEMCMP_EQUAL(&afterInit, &myMock, sizeof(RocketMock_t) );
}

TEST( RocketObject, test_my_mock_is_returned) {
    POINTERS_EQUAL(&myMock, Rocket(&myMock) );
}

/*------------ EXPECT -------------------------*/
TEST_GROUP_BASE(RocketMockExpect, RocketMockCore) {};

TEST( RocketMockExpect, test_expect_can_be_called_on_object) {

    //! [Setting expectation]
    EXPECTATION(expectOneNullByte, DATA({0u}));
    Rocket(&myMock)->expect(&expectOneNullByte);
    //! [Setting expectation]
}

TEST( RocketMockExpect, test_expect_cannot_give_null_parameter) {
    CHECK_THROWS(std::runtime_error, Rocket(&myMock)->expect(NULL));
}

/*------------ FEED -------------------------*/
TEST_GROUP_BASE(RocketMockFeed, RocketMockCore) {};

TEST( RocketMockFeed, test_feed_can_be_called_on_object) {
    //! [Feeding data]
    const uint8_t someData[] = {0u, 1u, 2u, 3u};
    Rocket(&myMock)->feed(someData, sizeof(someData));
    //! [Feeding data]
}

TEST( RocketMockFeed, test_feed_cannot_give_null_parameter) {
    CHECK_THROWS(std::runtime_error, Rocket(&myMock)->feed(NULL, 0u));
}

/*------------ FEED, EXPECT, CHECK -------------------*/
EXPECTATION(myFibonacci, DATA({1u, 1u, 2u, 3u, 5u, 8u}))
const uint8_t fibonacci[] = {1u, 1u, 2u, 3u, 5u, 8u};
TEST_GROUP_BASE(RocketMockFeedExpectCheck, RocketMockCore) {};

TEST( RocketMockFeedExpectCheck, test_no_data_fed_expectation_not_yet_met) {
    Rocket(&myMock)->expect(&myFibonacci);
    CHECK_EQUAL(ROCKET_EXPECTATION_NOT_MET, Rocket(&myMock)->check());
}

TEST( RocketMockFeedExpectCheck, test_all_data_fed_at_once_succeeds) {
    Rocket(&myMock)->expect(&myFibonacci);

    Rocket(&myMock)->feed(fibonacci, sizeof(fibonacci));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}

TEST( RocketMockFeedExpectCheck, test_data_fed_in_parts_succeeds) {
    Rocket(&myMock)->expect(&myFibonacci);

    Rocket(&myMock)->feed(&fibonacci[0u], 2u);
    Rocket(&myMock)->feed(&fibonacci[2u], 2u);
    Rocket(&myMock)->feed(&fibonacci[4u], 2u);

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}


TEST( RocketMockFeedExpectCheck, test_with_not_all_data_expectation_check_returns_not_met) {
    Rocket(&myMock)->expect(&myFibonacci);

    Rocket(&myMock)->feed(&fibonacci[0u], 2u);

    CHECK_EQUAL(ROCKET_EXPECTATION_NOT_MET, Rocket(&myMock)->check());
}

TEST( RocketMockFeedExpectCheck, test_with_wrong_data_then_expectation_is_not_met) {
    Rocket(&myMock)->expect(&myFibonacci);

    Rocket(&myMock)->feed(&fibonacci[0u], 2u);
    Rocket(&myMock)->feed(&fibonacci[0u], 2u);

    CHECK_EQUAL(ROCKET_EXPECTATION_NOT_MET, Rocket(&myMock)->check());
}

TEST( RocketMockFeedExpectCheck, test_with_too_much_data_then_expectation_is_met) {
    Rocket(&myMock)->expect(&myFibonacci);

    Rocket(&myMock)->feed(fibonacci, sizeof(fibonacci));
    Rocket(&myMock)->feed(fibonacci, sizeof(fibonacci));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}

/*------------ Multiple expectations ---------*/
TEST_GROUP_BASE(RocketMockFeedMultiExpectCheck, RocketMockCore) {};

TEST( RocketMockFeedMultiExpectCheck, test_multiple_expectations_can_be_set) {

    EXPECTATION(firstExpectation, DATA({1u, 2u, 3u}))
    EXPECTATION(secondExpectation, DATA({4u, 5u, 6u}))

    const uint8_t actualData[] = {1u, 2u, 3u, 4u, 5u, 6u};

    Rocket(&myMock)->expect(&firstExpectation);
    Rocket(&myMock)->expect(&secondExpectation);

    Rocket(&myMock)->feed(actualData, sizeof(actualData));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}

TEST( RocketMockFeedMultiExpectCheck, test_multiple_expectations_with_again_can_be_set) {

    EXPECTATION(firstExpectation, DATA({1u, 2u}))
    EXPECTATION(secondExpectation, DATA({3u, 4u}))

    const uint8_t actualData[] = {1u, 2u, 1u, 2u, 3u, 4u};

    Rocket(&myMock)->expect(&firstExpectation)->again();
    Rocket(&myMock)->expect(&secondExpectation);

    Rocket(&myMock)->feed(actualData, sizeof(actualData));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}

TEST( RocketMockFeedMultiExpectCheck, test_same_expectation_again_fails)
{
    EXPECTATION(firstExpectation, DATA({1u, 2u}))
    EXPECTATION(secondExpectation, DATA({3u, 4u}))

    const uint8_t actualData[] = {1u, 2u, 3u, 4u};

    Rocket(&myMock)->expect(&firstExpectation)->again();
    Rocket(&myMock)->expect(&secondExpectation);

    Rocket(&myMock)->feed(actualData, sizeof(actualData));  

    CHECK_EQUAL(ROCKET_EXPECTATION_NOT_MET, Rocket(&myMock)->check());               
}

/*------------ AGAIN -------------------------*/
EXPECTATION(myRepetitiveExpectation, DATA({1u}))
const uint8_t myActualRepetitiveData[] = {1u, 1u};
TEST_GROUP_BASE(RocketMockAgain, RocketMockCore) {};

TEST( RocketMockAgain, test_again_is_working) {

    Rocket(&myMock)->expect(&myRepetitiveExpectation)->again();

    Rocket(&myMock)->feed(myActualRepetitiveData, sizeof(myActualRepetitiveData));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}

TEST( RocketMockAgain, test_again_is_working_multiple_times) {

    Rocket(&myMock)->expect(&myRepetitiveExpectation)->again()->again()->again();

    Rocket(&myMock)->feed(myActualRepetitiveData, sizeof(myActualRepetitiveData));
    Rocket(&myMock)->feed(myActualRepetitiveData, sizeof(myActualRepetitiveData));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}

TEST( RocketMockAgain, test_expectation_not_met_when_only_first_iteration_is_met) {

    Rocket(&myMock)->expect(&myRepetitiveExpectation)->again();

    Rocket(&myMock)->feed(myActualRepetitiveData, 1u);

    CHECK_EQUAL(ROCKET_EXPECTATION_NOT_MET, Rocket(&myMock)->check());
}

/*------------ NTIMES -------------------------*/
TEST_GROUP_BASE(RocketMockNTimes, RocketMockCore) {};

TEST( RocketMockNTimes, test_ntimes_is_working) {

    Rocket(&myMock)->expect(&myRepetitiveExpectation)->n_times(1u);

    Rocket(&myMock)->feed(myActualRepetitiveData, sizeof(myActualRepetitiveData));

   CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}

TEST( RocketMockNTimes, test_ntimes_is_working_multiple_times) {

    Rocket(&myMock)->expect(&myRepetitiveExpectation)->n_times(3u);

    Rocket(&myMock)->feed(myActualRepetitiveData, sizeof(myActualRepetitiveData));
    Rocket(&myMock)->feed(myActualRepetitiveData, sizeof(myActualRepetitiveData));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}

TEST( RocketMockNTimes, test_expectation_not_met_when_only_first_iteration_is_met) {

    Rocket(&myMock)->expect(&myRepetitiveExpectation)->n_times(2u);

    Rocket(&myMock)->feed(myActualRepetitiveData, 1u);

    CHECK_EQUAL(ROCKET_EXPECTATION_NOT_MET, Rocket(&myMock)->check());
}

/*------------ RESET -------------------------*/
TEST_GROUP_BASE(RocketMockReset, RocketMockCore) {};

TEST( RocketMockReset, test_reset_can_be_called_on_object) {
    Rocket(&myMock)->reset();
}

TEST( RocketMockReset, test_reset_returns_object) {
    POINTERS_EQUAL(&myMock, Rocket(&myMock)->reset() );
}

TEST( RocketMockReset, test_will_call_on_last_set_rocket) {

    EXPECTATION(myExpectation, DATA({0u}));
    Rocket(&myMock)->expect(&myExpectation);

    Rocket(&myMock)->reset();

    CHECK_EQUAL(ROCKET_EXPECTATION_UNSET, Rocket(&myMock)->check());
}

/*------------ REACTION -------------------------*/
static RocketMockResult_t myDummyReaction(RocketMockBuffer_t const * const bufIn, RocketMockBuffer_t * const bufOut) {
    *bufOut = *bufIn;
    return ROCKET_NO_ERROR;
}

static RocketMockBuffer_t myExternalBuffer;
static size_t sunkData = 0u;

static void myDummySink(RocketMockBuffer_t const * const buffer) {
    myExternalBuffer = *buffer;
    sunkData += buffer->size;
}

TEST_GROUP_BASE(RocketMockReactions, RocketMockCore) {

    void setup() {
        sunkData = 0u;
        RocketMockCore::setup();
    }
};

TEST( RocketMockReactions, test_reaction_can_be_set)
{
    EXPECTATION(myExpectation, DATA({0u, 1u}));
    uint8_t actualData[] = {0u, 1u};

    Rocket(&myMock)->set_sink(&myDummySink);

    Rocket(&myMock)->expect(&myExpectation)->will(REACT(myDummyReaction));

    Rocket(&myMock)->feed(actualData, sizeof(actualData));

    CHECK_EQUAL( myExternalBuffer.data, myExpectation._matcher.buf.data );
    CHECK_EQUAL( myExternalBuffer.size, myExpectation._matcher.buf.size );
}

TEST( RocketMockReactions, test_reaction_will_be_repeated)
{
    EXPECTATION(myExpectation, DATA({0u, 1u}));
    uint8_t actualData[] = {0u, 1u};    

    Rocket(&myMock)->set_sink(&myDummySink);

    Rocket(&myMock)->expect(&myExpectation)->will(REACT(myDummyReaction))->again();

    Rocket(&myMock)->feed(actualData, sizeof(actualData));
    Rocket(&myMock)->feed(actualData, sizeof(actualData));

    CHECK_EQUAL( 2u * sizeof(actualData), sunkData );
}

/*--------- ExpectationHierarchy ------------*/

//! [ExpectationHierarchy1]
EXPECTATION(expectation1, DATA({0u, 1u}));
EXPECTATION(expectation1a, DATA({2u, 3u}));
EXPECTATION(expectation1b, DATA({4u, 5u}));
EXPECTATION(expectation2a, DATA({2u, 5u}));
EXPECTATION(expectation2b, DATA({4u, 5u}));
EXPECTATION(expectation3, DATA({0u, 2u}));
EXPECTATION(expectation4, DATA({0u, 3u}));
//! [ExpectationHierarchy1]
TEST_GROUP_BASE(RocketMockExpectationHierarchy, RocketMockCore) {

    void setup() {
        resetExpectations();
        setupExpectations();
    }

    void setupExpectations() {
    //! [ExpectationHierarchy2]
        Rocket(&myMock)->expect(&expectation1);
        Rocket(&myMock)->expect(&expectation1a)->after(&expectation1);
        Rocket(&myMock)->expect(&expectation1b)->after(&expectation1a);
        Rocket(&myMock)->expect(&expectation2a)->after(&expectation1);
        Rocket(&myMock)->expect(&expectation2b)->after(&expectation2a);
        Rocket(&myMock)->expect(&expectation3);
        Rocket(&myMock)->expect(&expectation4);
    //! [ExpectationHierarchy2]
    }

    void resetExpectations() {
        resetExpectation(&expectation1);
        resetExpectation(&expectation1a);
        resetExpectation(&expectation1b);
        resetExpectation(&expectation2a);
        resetExpectation(&expectation2b);     
        resetExpectation(&expectation3);  
        resetExpectation(&expectation4);             
    }

    void resetExpectation(RocketMockExpectationData_t* expectation) {
        expectation->_matcher.currentPosition = expectation->_matcher.buf.data;
        expectation->_state = ROCKET_EXPECTATION_CANDIDATE;
    }
};

TEST( RocketMockExpectationHierarchy, test_only_exp1_met)
{
    Rocket(&myMock)->feed(expectation1_data, sizeof(expectation1_data));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation1));
}

TEST( RocketMockExpectationHierarchy, test_exp1a_after_exp1_met)
{
    Rocket(&myMock)->feed(expectation1_data, sizeof(expectation1_data));
    Rocket(&myMock)->feed(expectation1a_data, sizeof(expectation1a_data));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation1));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation1a));
}

TEST( RocketMockExpectationHierarchy, test_exp1_after_exp1a_not_met)
{
    Rocket(&myMock)->feed(expectation1a_data, sizeof(expectation1a_data));
    Rocket(&myMock)->feed(expectation1_data, sizeof(expectation1_data));

    CHECK_EQUAL(ROCKET_EXPECTATION_NOT_MET, Rocket(&myMock)->checkExpectation(&expectation1));
    CHECK_EQUAL(ROCKET_EXPECTATION_CANDIDATE, Rocket(&myMock)->checkExpectation(&expectation1a));
}

TEST( RocketMockExpectationHierarchy, test_exp3_before_exp1_met)
{
    Rocket(&myMock)->feed(expectation3_data, sizeof(expectation3_data));    
    Rocket(&myMock)->feed(expectation1_data, sizeof(expectation1_data));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation1));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation3));
}

TEST( RocketMockExpectationHierarchy, test_exp4_before_exp1_met)
{
    Rocket(&myMock)->feed(expectation4_data, sizeof(expectation4_data));    
    Rocket(&myMock)->feed(expectation1_data, sizeof(expectation1_data));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation1));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation4));
}

TEST( RocketMockExpectationHierarchy, test_exp4_and_exp3_before_exp1_met)
{
    Rocket(&myMock)->feed(expectation4_data, sizeof(expectation4_data));    
    Rocket(&myMock)->feed(expectation3_data, sizeof(expectation3_data));    
    Rocket(&myMock)->feed(expectation1_data, sizeof(expectation1_data));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation1));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation3));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation4));
}

TEST( RocketMockExpectationHierarchy, test_two_non_sequential_branches)
{
    Rocket(&myMock)->feed(expectation1_data, sizeof(expectation1_data));    
    Rocket(&myMock)->feed(expectation1a_data, sizeof(expectation1a_data));    
    Rocket(&myMock)->feed(expectation2a_data, sizeof(expectation2a_data));
    Rocket(&myMock)->feed(expectation1b_data, sizeof(expectation1b_data));
    Rocket(&myMock)->feed(expectation2b_data, sizeof(expectation2b_data));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation1));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation1a));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation1b));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation2a));
    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->checkExpectation(&expectation2b));
}

TEST( RocketMockExpectationHierarchy, test_branch_turn_by_turn)
{
    Rocket(&myMock)->feed(expectation1_data, sizeof(expectation1_data));    
    Rocket(&myMock)->feed(expectation2a_data, sizeof(expectation2a_data));
    Rocket(&myMock)->feed(expectation1a_data, sizeof(expectation1a_data));    
    Rocket(&myMock)->feed(expectation4_data, sizeof(expectation4_data));
    Rocket(&myMock)->feed(expectation1b_data, sizeof(expectation1b_data));
    Rocket(&myMock)->feed(expectation3_data, sizeof(expectation3_data));
    Rocket(&myMock)->feed(expectation2b_data, sizeof(expectation2b_data));

    CHECK_EQUAL(ROCKET_EXPECTATION_MET, Rocket(&myMock)->check());
}

int main(int ac, char** av)
{
   return CommandLineTestRunner::RunAllTests(ac, av);
}